import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  ep: string = "https://mc.appifi.io/_endpoints/app/"; // commands will be in the structure of "account/sign-in", or "appointments/list", etc.
  httpOptions: any = {
    headers: new HttpHeaders()
  }
  token: any;
  user: any;
  company: any;
  constructor(public http: HttpClient, private alertCtrl: AlertController) { }
  // SAVE DATA TO LOCAL STORAGE
  save(key: string, data: any) {
    localStorage.setItem(key, JSON.stringify(data));
  }

  // LOAD DATA FROM LOCAL STORAGE
  load(key: string) {
    if (localStorage.getItem(key)) {
      var stringifiedData = localStorage.getItem(key);
      return JSON.parse(stringifiedData);
    } else {
      return null;
    }
  }

  // ALERT SERVER OR INTERNET CONNECTION ERROR
  dataError(res) {
    let alert = this.alertCtrl.create({
      header: 'Data Error',
      message: JSON.stringify(res),
      buttons: ['CLOSE']
    }).then(alert => alert.present());
  }

  // ALERT SERVER OR INTERNET CONNECTION ERROR
  serverError() {
    let alert = this.alertCtrl.create({
      header: 'Server Error',
      message: 'Unable to Connect to Server. Please Check Your Internet Connection',
      buttons: ['CLOSE']
    }).then(alert => alert.present());
  }

  // SETS THE TOKEN IN THE HTTP HEADERS
  setToken(token:string = null) {
    if(token) {
      this.httpOptions = {
        headers: new HttpHeaders().set('X-Auth-Token', token)
      }
    } else {
      console.log(1);
      this.httpOptions = {
        headers: new HttpHeaders()
      }
      console.log(this,this.httpOptions);
    }
  }

  // GET DATA FROM SERVER
  get(command: string, data: any = null, triggerError: boolean = true) {
    let args = '?';
    if (data) {
      for (var key in data) {
        args += key + '=' + data[key] + '&';
      }
    }
    if(this.load('token')) {
      this.httpOptions = {
        headers: new HttpHeaders().set('X-Auth-Token', this.load('token'))
      }
    } else {
      this.httpOptions = {
        headers: new HttpHeaders()
      }
    }
    return new Promise((resolve, reject) => {
      this.http.get(this.ep + command + args, this.httpOptions).toPromise().then((response: any) => {
        if (triggerError) {
          if (response.result == 'success') resolve(response);
          else this.dataError(response);
        } else {
          resolve(response);
        }
      }).catch((error) => {
        if (triggerError) this.serverError();
      });
    });
  }

  //SIGNOUT
signOut(){
  localStorage.clear();
  console.log('Done!');
}

  // POST DATA TO SERVER
  post(command, data, triggerError: boolean = true) {
    if(this.load('token')) {
      this.httpOptions = {
        headers: new HttpHeaders().set('X-Auth-Token', this.load('token'))
      }
    } else {
      this.httpOptions = {
        headers: new HttpHeaders()
      }
    }
    return new Promise((resolve, reject) => {
      this.http.post(this.ep + command, JSON.stringify(data), this.httpOptions).toPromise().then((response: any) => {
        if (triggerError) {
          if (response.result == 'success') resolve(response);
          else this.dataError(response);
        } else {
          resolve(response);
        }
      }).catch((error) => {
        if (triggerError) this.serverError();
      });
    });
  }
}
