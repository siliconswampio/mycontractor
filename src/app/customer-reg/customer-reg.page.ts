import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-customer-reg',
  templateUrl: './customer-reg.page.html',
  styleUrls: ['./customer-reg.page.scss'],
})
export class CustomerRegPage implements OnInit {
  user:any = {
    first_name: '',
    last_name: '',
    email_address: '',
    phone_number: '',
    password: ''
  };
  passwords: any = {
    password1: '',
    password2: ''
  }
  success: string;
  registrationError: string;

  constructor(
    public ds: DataService,
    public router: Router,
    public menuCtrl: MenuController,
  ) { }

  ngOnInit() {
  }
  
  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }
  
  register() {
    if (this.passwords.password2 == this.passwords.password1) {
      this.user.password = this.passwords.password1;
    }
    let data = this.user;
    console.log('customer registeration check', data);
    this.ds.post('customer/register', data, false).then((data:any) => {
      console.log('customer registration data', data);
      if (data.result == 'failure') {
        this.registrationError = data.error;
        console.log('failure');
      }
      else {
        this.ds.save('token', data.token);
        this.ds.setToken(data.token);
        console.log('this was successful');
        this.success = "Your registration was successful, please check your email for verification!";
        this.router.navigateByUrl('/customer-dashboard');
        this.menuCtrl.enable(true); //menu was showing false on redirect to dash so if login success, it makes it be true
      }
    });
  }
}
