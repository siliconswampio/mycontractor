import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.page.html',
  styleUrls: ['./sign-in.page.scss'],
})
export class SignInPage implements OnInit {
  user: any = this.ds.load('user') ? this.ds.load('user') : {
    email_address: '',
    phone_number: '',
    password: '',
  };
  registrationError:string;
  success: string;
  remember_me: boolean = false;
  token: any = this.ds.load('token');

  constructor(
    public ds: DataService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public menuCtrl: MenuController,
  ) { }

  ngOnInit() {
    if(this.ds.load('remember')) {
      // this.remember_me = true;
      // this.signIn();
      this.ds.setToken(this.token);
      this.ds.save('token', this.token);
      if (this.user.user_type && this.user.user_type == 'Provider') {
        this.menuCtrl.enable(true);
        this.router.navigate(['dashboard']);
      } else {
        this.menuCtrl.enable(true);
        this.router.navigate(['customer-dashboard']);
      }
    } 
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }

  getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  signIn() {
    console.log('sign-in check', this.user);
    if(!this.user.email_address && !this.user.phone_number && !this.user.password) {
      this.registrationError = 'Please fill out All fields to sign in.';
      console.log("Fill out all the feilds!");
    }
    let data = this.user;
    this.ds.post('account/sign_in', data, false).then((data:any) => {
      console.log('sign_in data', data);
      if (data.result=="failure") {
        this.registrationError = data.error;
        console.log(data.error); 
      }
      else {
        if(this.remember_me) {
          this.ds.save('remember',true);
        }
        this.ds.save('token',data.token);
        this.ds.user = data.user;
        console.log('this.ds.user', this.ds.user);
        this.ds.save('user', data.user);
        if (data.user.company) {
          data.user.company['id'] = data.user.company_id;
          this.ds.company = data.user.company;
          this.ds.save('company', data.user.company);
        }
        this.success = data.success;
        console.log('data.user_type', data.user_type);
        if (data.user.user_type && data.user.user_type == 'Provider') {
          this.menuCtrl.enable(true);
          this.router.navigate(['dashboard']);
        } else {
          this.menuCtrl.enable(true);
          this.router.navigate(['customer-dashboard']);
        } //menu was showing false on redirect to dash so if login success, it makes it be true
        console.log('this was successful');
      }
    });
  }

}
