import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { DataService } from '../data.service';
import { ScheduleFeildsComponent } from '../components/schedule-feilds/schedule-feilds.component';

@Component({
  selector: 'app-appointment',
  templateUrl: './appointment.page.html',
  styleUrls: ['./appointment.page.scss'],
})
export class AppointmentPage implements OnInit {

  date: any;

   // jobs:any;
   jobs: any = [
    {
      job_id: 12,
      job_type: 'gardening',
      job_title: 'Build me a garden',
      description: 'I want a garden in my back yard to be able to grow fruits and vegetables, as well as flowers. I would like the beds to be raised, and cover most of the back yard, with connected stone paths around them all.',
      job_address: '128 15th Ave',
      job_address_2: '',
      job_city: 'Gotham City',
      job_state: 'NY',
      job_zip: '12010',
      job_location_type: 'residental', // might also be a drop down of choices
      status: 'Taking Bids', // might also be a set list of statuses, also be set on the back end
      date: '9/9/21',
    }
  ];

  constructor(
    public ds: DataService,
    public modalCtrl: ModalController,
  ) { }

  ngOnInit() {
    let dateFormate = new Date();

    this.date = dateFormate.toLocaleString([], {year: 'numeric', month: 'numeric', day: 'numeric'});
    console.log(this.date);
  
  }



  async openScheduler(job_id) {
    let modal = await this.modalCtrl.create({
      component: ScheduleFeildsComponent,
      swipeToClose: true,
      componentProps: {
        job_id: job_id
      }
    })
    
    modal.present()

    modal.onDidDismiss()
    .then((response) => {
      console.log(response.data);
      this.jobs.push({
        job_title: response.data.jobTitle,
        date: response.data.jobDate
      })
  });
  }

}
