import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { ModalController } from '@ionic/angular';
import { JobDetailComponent } from '../components/job-detail/job-detail.component';


@Component({
  selector: 'app-job-list',
  templateUrl: './job-list.page.html',
  styleUrls: ['./job-list.page.scss'],
})

export class JobListPage implements OnInit {
  
  // jobs:any;
  jobs: any = [
    {
      job_id: 12,
      job_type: 'gardening',
      job_title: 'Build me a garden',
      description: 'I want a garden in my back yard to be able to grow fruits and vegetables, as well as flowers. I would like the beds to be raised, and cover most of the back yard, with connected stone paths around them all.',
      job_address: '128 15th Ave',
      job_address_2: '',
      job_city: 'Gotham City',
      job_state: 'NY',
      job_zip: '12010',
      job_location_type: 'residental', // might also be a drop down of choices
      status: 'Taking Bids' // might also be a set list of statuses, also be set on the back end
    },
    {
      job_id: 11,
      job_type: 'building',
      job_title: 'Kitchen redo',
      description: 'I need a new set of cabinets, a new island, and granite countertops.',
      job_address: '125 10th Ave',
      job_address_2: '',
      job_city: 'Gotham City',
      job_state: 'NY',
      job_zip: '12010',
      job_location_type: 'residental', // might also be a drop down of choices
      status: 'Taking Bids' // might also be a set list of statuses, also be set on the back end
    },
    {
      job_id: 10,
      job_type: 'roofing',
      job_title: 'Updating',
      description: 'I need to redo my roof for insurance',
      job_address: '2019 Manatee Ave',
      job_address_2: '',
      job_city: 'Bradenton',
      job_state: 'Fl',
      job_zip: '34238',
      job_location_type: 'rental', // might also be a drop down of choices
      status: 'Taking Bids' // might also be a set list of statuses, also be set on the back end
    }
  ];

  myArea: any = [
    {
      user_id:'1',
      user_location: 'Sarasota, FL'
    },
    {
      user_id:'1',
      user_location: 'Manatee, FL'
    },
    {
      user_id:'1',
      user_location: 'Tampa, FL'
    },
    {
    user_id:'1',
    user_location: 'St Pete, FL'
    }
  ];

  myFilters: any = [
    {
      user_id:'1',
      user_services:'Carpentry'
    },
    {
      user_id:'1',
      user_services:'Painting'
    },
    {
      user_id:'1',
      user_services:'Plumbing'
    }

  ];

  newArea: any;
  user: any = this.ds.load('user');


  constructor(
    public ds: DataService,
    public modalCtrl: ModalController,
  ) { }

  ngOnInit() {
    // this.ds.get('job_list').then((data:any) => {
    //   this.jobs = data.jobs;
    // });

  }

  addArea(){
    let user_id = this.user.id;
    let user_location = this.newArea;
    this.myArea.push({user_id, user_location});
    console.log(this.myArea);
  }
  
  openJob(job_id) {
    this.modalCtrl.create({
      component: JobDetailComponent,
      swipeToClose: true,
      componentProps: {
        job_id: job_id
      }
    }).then(modal => modal.present());
  }
}
