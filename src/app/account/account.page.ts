import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { AssessmentComponent } from '../components/assessment/assessment.component';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {
  businessTypeOptions: any = {
    header: 'Business Type',
    subHeader: 'Select your business\' primary work type'
  };

  stateOptions: any = {
    header: 'State'
  };

  user:any = this.ds.load('user') ?  this.ds.load('user') : {
    name: '',
    email: '',
    phone_number: '',
    password1: '',
    password2: '',
    user_type: 'customer'
  };

  company: any = this.ds.load('company') ? this.ds.load('company') : {
    address1: '',
    address2: '',
    city: '',
    company_name: '',
    logo: '',
    state: '',
    zip: ''
  };

  success: string;
  error: string;

  navbar: any = {
    info: true,
    payment: false,
    reviews: false,
    referrals: false,
    company: false
  }

  verification: any = {
    password1: '',
    password2: ''
  }
  work: any = {
    business_type: '',
    work_radius: '',
    phone_number: '',
    website: '',
  };

  license: any = {
    license_num: '',
    img: File
  };

  insurance: any = {
    agency_name: '',
    agent_name: '',
    agent_phone: '',
    policy_num: '',
    img: File
  };

  job: any = [
    {
      name: 'construction',
      id: 1,
    },
    {
      name: 'landscaping',
      id: 2,
    },
    {
      name: 'painting',
      id: 3
    },
    {
      name: 'pool building',
      id: 4
    },
    {
      name: 'ac/hvac',
      id: 5
    }
  ];

  state_types: any = ['FL', 'TX', 'NC', 'SC', 'GA'];

  states: any[] = [
    { name: 'FL' },
    { name: 'GA' },
    { name: 'NC' }
  ];

  payments: any[] = [
    {id: 1, date: '2021/02/02', job_title: 'Build me a pool', job_id: 12, amount: '1200', status: 'paid'},
    {id: 2, date: '2021/02/03', job_title: 'Fixing roof', job_id: 13, amount: '100', status: 'pending'},
    {id: 3, date: '2021/02/05', job_title: 'Deck', job_id: 14, amount: '1500', status: 'paid'},
  ];

  reviews: any[] = [
    {id: 1, date: '2021/02/02', img: '../../assets/john_smith.jpeg', reviewed_by: 'John Smith', review: 'This company was very professional, showed up on time, did the job well, and has great pricing!'},
    {id: 2, date: '2021/02/03', img: '../../assets/lacy_lace.jpeg', reviewed_by: 'Lacy lace', review: 'I was very pleased with the work that they did. They were respectful, thoughtful, and knowledgable about their field, and made me feel heard!'},
    {id: 3, date: '2021/02/05', img: '../../assets/amber_brown.jpeg', reviewed_by: 'Amber Brown', review: 'They did good work, and left the place clean and put together!'}
  ];

  referrals: any[] = [
    {id: 1, date: '2021/02/02', img: '../../assets/john_smith.jpeg', refered_by: 'John Smith', referral: 'This company was very professional, showed up on time, did the job well, and has great pricing!'},
    {id: 2, date: '2021/02/03', img: '../../assets/lacy_lace.jpeg', refered_by: 'Lacy lace', referral: 'I was very pleased with the work that they did. They were respectful, thoughtful, and knowledgable about their field, and made me feel heard!'},
    {id: 3, date: '2021/02/05', img: '../../assets/amber_brown.jpeg', refered_by: 'Amber Brown', referral: 'They did good work, and left the place clean and put together!'}
  ];



  constructor(
    public ds: DataService,
    public modalCtrl: ModalController
  ) { }

  ngOnInit() {
    console.log('this.ds.user account page', this.user);
    this.ds.get('company/info', null, false).then((data:any) => {
      console.log('data',data);
      if(data.result == 'success') {
        this.company = data.company;
      }
      else {
        this.error = data.error;
      }
    });
  }

  // when the business type is selected there will be a modal that has the assessment on it
  openAssessment() {
    this.modalCtrl.create({
      component: AssessmentComponent,
      swipeToClose: true,
      componentProps: {
        assessment_type: this.company.business_type.id
      }
    }).then(modal => modal.present());
  }

  addActive(tab) {
    Object.keys(this.navbar).forEach(key => {
      if(this.navbar[key] == true) {
        this.navbar[key] = false;
      }
    });
    this.navbar[tab] = true;
  }

  updateAccount() {
    console.log('updating account', this.user);
    // this.ds.post('update_account', this.user).then((data:any) => {
    //   if (data.success) {
    //     this.success = data.success ? data.success : 'Your account has been updated successfully!';
    //     this.user = data.user;
    //   }
    //   else {
    //     this.error = data.error ? data.error : 'There was an error updating your account, please try again!';
    //   }
    // });
  }

  updateCompany() {
    console.log('company info updated');
  }

}
