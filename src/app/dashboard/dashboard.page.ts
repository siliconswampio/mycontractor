import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  activeJobs: any = [
    {
      job_id: 12,
      job_type: 'Build me a garden',
      status: 'In progress'
    },
    {
      job_id: 11,
      job_type: 'Kitchen redo',
      status: 'In progress'
    }
  ];

  closedJobs: any = [
    {
      job_id:13,
      job_type:'Paint cabinets',
      status: 'Closed'
    },
    {
      job_id:14,
      job_type:'Fix garage door',
      status: 'Closed'
    },
    {
      job_id:15,
      job_type:'Build shower',
      status: 'Closed'
    }
  ];

  userName: any = [
    {
      user_name: "Justin",
      user_id: 16
    }
  ];


  constructor(
    public ds: DataService,
    public router: Router,
  ) { }


  ngOnInit() {
    // this.ds.get('jobs', {user_id: this.ds.user.id}).then((data)=> {
    //   this.jobs = data.jobs;
    // });
  }

  btnClicked(page){
    // console.log(page);
    this.router.navigateByUrl(page);
  }
  

}
