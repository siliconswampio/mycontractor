import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-messenger',
  templateUrl: './messenger.page.html',
  styleUrls: ['./messenger.page.scss'],
})
export class MessengerPage implements OnInit {
  messages: any = this.ds.load('messages');
  new_message: any;
  recipent: any = {
    user_id: 14,
    name: 'John Smithy'
  };

  sides: any = {
    right: false,
    left: true,
  };

  conversations: any = [
    {name: 'Bill Leeson', id: 1, active: true},
    {name: 'John Smithy', id: 2},
    {name: 'Jane Doe', id: 3}
  ]
  active_convo: any = this.conversations[0];

  constructor(
    public ds: DataService,
  ) { }

  ngOnInit() {
  }

  // need to have endpoints constantly checking for new messages

  /*
  send() {
    let data = {
      message: this.new_message,
      user: this.ds.user.user_id,
      sent_to: this.recpient.user_id
    };
    this.ds.post('send_message', data).then((data:any) => {
      this.messages = data.messages;
    });
  }
  */

  // sends the message to the intended recipient
  sendMessage() {
    console.log('sent message', this.new_message);
  }

  // gets the conversation between the signed in user, and the selected account
  // note: will probaly have a endpoint call
  getConversation(id) {
    console.log('getting conversation with', id);
    for(let convo of this.conversations) {
      if (convo.id == id) {
        convo['active'] = true;
        this.active_convo = convo;
      } else {
        convo['active'] = false;
      }
    }
    this.changeSides();
  }

  openInfo() {
    console.log('getting info on active convo', this.active_convo.name, this.active_convo.id);
  }

  changeSides() {
    console.log(this.sides);
    if(this.sides.right) {
      this.sides.right = false;
      this.sides.left = true;
    } else {
      this.sides.left = false;
      this.sides.right = true;
    }
  }

}
