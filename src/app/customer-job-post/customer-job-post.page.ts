import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-customer-job-post',
  templateUrl: './customer-job-post.page.html',
  styleUrls: ['./customer-job-post.page.scss'],
})
export class CustomerJobPostPage implements OnInit {
  job: any = {
    job_type: '',
    job_title: '',
    description: '', // this needs to be a text area
    pictures: '', // needs to be  files
    job_address: '',
    job_address_2: '',
    job_city: '',
    job_state: '',
    job_zip: '',
    job_location_type: '', // might also be a drop down of choices
    status: '' // might also be a set list of statuses, also be set on the back end
  };
  postError: string;
  success: string;
  job_types: any = ['construction', 'landscaping', 'painting', 'pool building', 'ac/hvac']; // should probably be a drop down

  constructor(
    public ds: DataService,
  ) { }

  ngOnInit() {
    // this.ds.get('job_type').then((data:any) => {
    //   if(data.job_types) {
    //     this.job_types = data.job_types;
    //   }
    // });
  }

  post() {
    // this.ds.post('customer_job_posting', this.job).then((data:any) => {
    //   if (data.error) {
    //     this.postError = data.error;
    //   }
    //   else {
    //     this.success = 'Job posted successfully';
    //     
    //   }
    // });
  }

}
