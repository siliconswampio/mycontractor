import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomerJobPostPage } from './customer-job-post.page';

const routes: Routes = [
  {
    path: '',
    component: CustomerJobPostPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CustomerJobPostPageRoutingModule {}
