import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CustomerJobPostPage } from './customer-job-post.page';

describe('CustomerJobPostPage', () => {
  let component: CustomerJobPostPage;
  let fixture: ComponentFixture<CustomerJobPostPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerJobPostPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CustomerJobPostPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
