import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CustomerJobPostPageRoutingModule } from './customer-job-post-routing.module';

import { CustomerJobPostPage } from './customer-job-post.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomerJobPostPageRoutingModule
  ],
  declarations: [CustomerJobPostPage]
})
export class CustomerJobPostPageModule {}
