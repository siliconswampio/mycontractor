import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [

  {
    path: 'choose-reg',
    loadChildren: () => import('./choose-reg/choose-reg.module').then( m => m.ChooseRegPageModule)
  },
  {
    path: 'sign-in',
    loadChildren: () => import('./sign-in/sign-in.module').then( m => m.SignInPageModule)
  },
  {
    path: 'customer-reg',
    loadChildren: () => import('./customer-reg/customer-reg.module').then( m => m.CustomerRegPageModule)
  },
  {
    path: 'contractor-reg',
    loadChildren: () => import('./contractor-reg/contractor-reg.module').then( m => m.ContractorRegPageModule)
  },
  {
    path: 'customer-job-post',
    loadChildren: () => import('./customer-job-post/customer-job-post.module').then( m => m.CustomerJobPostPageModule)
  },
  {
    path: 'job-list',
    loadChildren: () => import('./job-list/job-list.module').then( m => m.JobListPageModule)
  },
  {
    path: 'messenger',
    loadChildren: () => import('./messenger/messenger.module').then( m => m.MessengerPageModule)
  },
  {
    path: 'account',
    loadChildren: () => import('./account/account.module').then( m => m.AccountPageModule)
  },
  {
    path: 'job-post',
    loadChildren: () => import('./job-post/job-post.module').then( m => m.JobPostPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'customer-dashboard',
    loadChildren: () => import('./customer-dashboard/customer-dashboard.module').then( m => m.CustomerDashboardPageModule)
  },
  {
    path: 'my-jobs',
    loadChildren: () => import('./my-jobs/my-jobs.module').then( m => m.MyJobsPageModule)
  },
  {
    path: '',
    redirectTo:'choose-reg',
    pathMatch:'full',
    // loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },  {
    path: 'appointment',
    loadChildren: () => import('./appointment/appointment.module').then( m => m.AppointmentPageModule)
  }


];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
