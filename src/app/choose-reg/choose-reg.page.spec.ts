import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ChooseRegPage } from './choose-reg.page';

describe('ChooseRegPage', () => {
  let component: ChooseRegPage;
  let fixture: ComponentFixture<ChooseRegPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChooseRegPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ChooseRegPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
