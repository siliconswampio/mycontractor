import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChooseRegPageRoutingModule } from './choose-reg-routing.module';

import { ChooseRegPage } from './choose-reg.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChooseRegPageRoutingModule
  ],
  declarations: [ChooseRegPage]
})
export class ChooseRegPageModule {}
