import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChooseRegPage } from './choose-reg.page';

const routes: Routes = [
  {
    path: '',
    component: ChooseRegPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChooseRegPageRoutingModule {}
