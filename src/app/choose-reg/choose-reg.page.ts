import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../data.service';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-choose-reg',
  templateUrl: './choose-reg.page.html',
  styleUrls: ['./choose-reg.page.scss'],
})
export class ChooseRegPage implements OnInit {

  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public ds: DataService,
    public menuCtrl: MenuController,
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }
  

}
