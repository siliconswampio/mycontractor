import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ContractorRegPage } from './contractor-reg.page';

describe('ContractorRegPage', () => {
  let component: ContractorRegPage;
  let fixture: ComponentFixture<ContractorRegPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorRegPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ContractorRegPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
