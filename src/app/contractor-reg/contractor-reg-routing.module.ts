import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ContractorRegPage } from './contractor-reg.page';

const routes: Routes = [
  {
    path: '',
    component: ContractorRegPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ContractorRegPageRoutingModule {}
