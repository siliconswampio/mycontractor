import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ContractorRegPageRoutingModule } from './contractor-reg-routing.module';

import { ContractorRegPage } from './contractor-reg.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ContractorRegPageRoutingModule
  ],
  declarations: [ContractorRegPage]
})
export class ContractorRegPageModule {}
