import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../data.service';
import { IonSlides, ModalController } from '@ionic/angular';
import { AssessmentComponent } from '../components/assessment/assessment.component';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-contractor-reg',
  templateUrl: './contractor-reg.page.html',
  styleUrls: ['./contractor-reg.page.scss'],
})

export class ContractorRegPage implements OnInit {

  @ViewChild('regSlider') slides: IonSlides;

  slideOpts = {
    initialSlide: 0,
  };

  businessTypeOptions: any = {
    header: 'Business Type',
    subHeader: 'Select your business\' primary work type'
  };

  stateOptions: any = {
    header: 'State'
  };

  user: any = {
    company_name: '',
    address1: '',
    address2: '',
    city: '',
    state: '',
    zip: '',
    first_name: '',
    last_name: '',
    phone_number: '',
    email_address: '',
    user_type: 'Provider'
  };

  company: any = {
    business_type: '',
    work_radius: '',
    phone_number: '',
    website: '',
  };

  license_insurance: any = {
    license_num: '',
    license_file: File,
    insurance_agency_name: '',
    insurance_agent_name: '',
    insurance_agent_phone: '',
    insurance_policy_num: '',
    insurance_file: File
  };

  verification: any = {
    number: '',
    password1: '',
    password2: ''
  };

  jobs: any;

  states: any[] = [
    { name: 'Alabama' },
    { name: 'Alaska' },
    { name: 'Arizona' },
    { name: 'Arkansas' },
    { name: 'California' },
    { name: 'Colorado' },
    { name: 'Connecticut' },
    { name: 'Delaware' },
    { name: 'Florida' },
    { name: 'Georgia' },
    { name: 'Hawaii' },
    { name: 'Idaho' },
    { name: 'Illinois' },
    { name: 'Indiana'},
    { name: 'Iowa' },
    { name: 'Kansas' },
    { name: 'Kentucky' },
    { name: 'Louisiana' },
    { name: 'Maine' },
    { name: 'Maryland' },
    { name: 'Massachusetts' },
    { name: 'Michigan' },
    { name: 'Minnesota' },
    { name: 'Mississippi' },
    { name: 'Missouri' },
    { name: 'MontanaNebraska' },
    { name: 'Nevada' },
    { name: 'New Hampshire' },
    { name: 'New Jersey' },
    { name: 'New Mexico' },
    { name: 'New York' },
    { name: 'North Carolina' },
    { name: 'North Dakota' },
    { name: 'Ohio' },
    { name: 'Oklahoma' },
    { name: 'Oregon' },
    { name: 'Pennsylvania' },
    { name: 'Rhode Island'},
    { name: 'South Carolina' },
    { name: 'South Dakota' },
    { name: 'Tennessee' },
    { name: 'Texas' },
    { name: 'Utah' },
    { name: 'Vermont' },
    { name: 'Virginia' },
    { name: 'Washington' },
    { name: 'West Virginia' },
    { name: 'Wisconsin' },
    { name: 'Wyoming' }
  ];

  errors: any = [];

  registrationError: string;
  success: string;
  loading:  boolean = false;
  constructor(
    public ds: DataService,
    public modalCtrl: ModalController,
    public router: Router,
    public menuCtrl: MenuController
  ) { }

  ngOnInit() {
    // will only work if there's a token in the header...
    this.ds.get('surveys/list', null, false).then((data:any) => {
      this.jobs = data.surveys;
    });
  }
  ionViewWillEnter() {
    this.menuCtrl.enable(false);
  }
  // when the business type is selected there will be a modal that has the assessment on it
  openAssessment() {
    this.modalCtrl.create({
      component: AssessmentComponent,
      swipeToClose: true,
      componentProps: {
        assessment_type: this.company.business_type
      }
    }).then(modal => modal.present());
  }

  register(slide_num) {
    this.errors = [];
    // this.loading = true;
    // steps needed: 
    // - start loading circle when clicked, stop it when the data comes back
    let data = {};
    switch (slide_num) {
      // this is for general information on the company and owner
      case 1: {
        data = this.user;
        for(let key in data) {
          console.log(key, data[key]);
          if (data[key] == '' && key != 'address2') {
            this.errors.push(this.capitalizeKey(key) + ' is required, please fill it out!');
          }
        }
        if (Object.keys(this.errors).length > 0) {
          console.log('there are errors: ', this.errors);
          break;
        }
        else {
          console.log('there arent errors');
          // send user data to endpoint and then got to the next slide
          this.ds.post('account/register', data, false).then((data:any) => {
            console.log('first contractor register data, ', data);
            if (data.result == 'failure') {
              for(let error of data.errors) {
                this.errors.push(error);
              }
            } else {
              this.ds.save('token', data.token);
              this.user.user_id = data.user_id;
              this.user.company = data.company_id;
              this.ds.save('user', this.user);
              this.ds.get('surveys/list', null, false).then((data:any) => {
                this.jobs = data.surveys;
              });
              this.slides.slideNext(300);
            }
          });
          break;
        }
      }
      // this is the verification step
      case 2: {
        data = this.verification;
        for(let key in data) {
          console.log(key, data[key]);
          if (data[key] == '') {
            this.errors.push(this.capitalizeKey(key) + ' is required, please fill it out!');
          }
          if(data['password1'] != data['password2']) {
            this.errors.push('Passwords need to match');
          }
        }
        if (Object.keys(this.errors).length > 0) {
          console.log('there are errors: ', this.errors);
          break;
        }
        else {
          console.log('there arent errors');
          let sendData = {
            number: data['number'],
            password: data['password1']
          }
          // send user data to endpoint and then got to the next slide
          this.ds.post('company/verification', sendData, false).then((data:any) => {
            if (data.result == 'failure') {
              for (let error of data.errors) {
                this.errors.push(error);
              }
            } else {
              this.slides.slideNext(300);
            }
          });
          break;
        }
      }
      // this is company preferences
      case 3: {
        data = this.company;
        for(let key in data) {
          console.log(key, data[key]);
          if (data[key] == '') {
            this.errors.push(this.capitalizeKey(key) + ' is required, please fill it out!');
          }
        }
        if (Object.keys(this.errors).length > 0) {
          console.log('there are errors: ', this.errors);
          break;
        }
        else {
          console.log('there arent errors');
          // send user data to endpoint and then got to the next slide
          this.ds.post('company/update_preferences', data, false).then((data:any) => {
            if (data.result == 'failure') {
              for(let error of data.errors) {
                this.errors.push(error);
              }
            } else {
              this.slides.slideNext(300);
            }
          });
          break;
        }
      }
      // this is license and insurance
      case 4: {
        console.log('insurance', this.license_insurance);
        let formData = new FormData();
        for(let key in this.license_insurance) {
          if(key.includes('file')) {
            formData.append(key, new Blob(), this.license_insurance.key);
          }
          else {
            formData.append(key, this.license_insurance[key]);
          }
        }
        console.log('formdata', formData);
        data = this.license_insurance;
        for(let key in data) {
          console.log(key, data[key]);
          if (data[key] == '') {
            this.errors.push(this.capitalizeKey(key) + ' is required, please fill it out!');
          }
        }
        if (Object.keys(this.errors).length > 0) {
          console.log('there are errors: ', this.errors);
          break;
        }
        else {
          console.log('there arent errors insurance');
          // send user data to endpoint and then got to the next slide
          this.ds.post('company/license_insurance', formData, false).then((data:any) => {
            console.log('license/insurance stuff', data)
            if(data.result == 'failure') {
              for(let error in data.errors) {
                this.errors.push(error);
              }
            } else {
              this.menuCtrl.enable(true);
              this.router.navigateByUrl('/dashboard');
            }
          });
          break;
        }
      }
    }
  }

  capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  capitalizeKey(key) {
    let display_key = key.replace('_', ' ').split(' ');
    let final_display_key = this.capitalizeFirstLetter(display_key[0]) + ' ' + this.capitalizeFirstLetter(display_key[1]);
    return final_display_key;
  }

}

