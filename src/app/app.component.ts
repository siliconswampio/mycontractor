import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { MenuController } from '@ionic/angular';
import { DataService } from './data.service';
import { Router } from '@angular/router';

@Component({
	selector: 'app-root',
	templateUrl: 'app.component.html',
	styleUrls: ['app.component.scss']
})
export class AppComponent {
	user: any;
	messages: any;
	navigate: any;
	overlayHidden: boolean = false;
	constructor(
		private ds: DataService,
		private platform: Platform,
		private splashScreen: SplashScreen,
		private statusBar: StatusBar,
		private menu: MenuController,
		private router: Router,
	) {
		this.initializeApp();
		this.sideMenu();
	}
	initializeApp() {
		// gotta get user, and then get messages
		// then get bids?
		this.platform.ready().then(() => {
			this.statusBar.styleDefault();
			this.splashScreen.hide();
		});
	}

	sideMenu() {
		if (this.ds.load('user') && this.ds.load('user').user_type == 'Provider') {
			this.navigate = [
				{
					title: 'Dashboard',
					url: '/dashboard',
					icon: 'speedometer-outline'
				},
				{
					title: 'Find Jobs',
					url: '/job-list',
					icon: 'compass-outline'
				},
				{
					title: 'My Jobs',
					url: '/my-jobs',
					icon: 'construct'
				},
				{
					title: 'Messages',
					url: '/messenger',
					icon: 'chatbubbles-outline'
				},
				{
					title: 'My Account',
					url: '/account',
					icon: 'person-circle-outline'
				},
			];
		} else {
			this.navigate = [
				{
					title: 'Dashboard',
					url: '/customer-dashboard',
					icon: 'speedometer-outline'
				},
				{
					title: 'Find Jobs',
					url: '/job-list',
					icon: 'compass-outline'
				},
				{
					title: 'My Jobs',
					url: '/my-jobs',
					icon: 'construct'
				},
				{
					title: 'Messages',
					url: '/messenger',
					icon: 'chatbubbles-outline'
				},
				{
					title: 'My Account',
					url: '/account',
					icon: 'person-circle-outline'
				},
			];
		}
	}
	openEnd() {
		this.menu.close();
	}

	logOut() {
		this.ds.signOut();
		console.log('Done!');
		this.router.navigateByUrl('/sign-in');
	}
}