import { Component, Input, OnInit } from '@angular/core';
import { DataService } from '../../data.service';
import { ModalController, LoadingController, AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Network } from '@ionic-native/network/ngx';

@Component({
  selector: 'app-assessment',
  templateUrl: './assessment.component.html',
  styleUrls: ['./assessment.component.scss'],
})
export class AssessmentComponent implements OnInit {
  @Input() assessment_type: number;

  survey: any;
  success: any;
  error: any;

  queuedSurvey = false;
  submissionResponse:any = {};
  formData:any = {};
  form:any;
  files:any = {};
  fileList:any = [];

  constructor(
    public ds: DataService,
    public modal: ModalController,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private storage: Storage,
    private network: Network,
    private platform: Platform,
    private perms: AndroidPermissions,
  ) { }

  ngOnInit() {
    console.log('survey id', this.assessment_type);
    this.ds.post('surveys/load', {survey_id: this.assessment_type}, false).then((data:any) => {
      console.log('survey data', data);
      if(data.survey) {
        this.survey = data.survey;
        console.log('this.survey', this.survey);
      }
    });
  }

  submit() {
    // this.ds.get('submit_assessment').then((data:any) => {
    //   if(data.success) {
    //     this.success = data.success;
    //   }
    //   else {
//          this.error = data.error;
    // }
    // });
  }

  saveForm(){
    this.storage.ready().then(() => {
      this.storage.remove('savedForm');
      let form = {
        id: this.assessment_type,
        title: this.survey.title,
        form: this.form,
        formData: this.formData
      };
      this.storage.set('savedForm', JSON.stringify(form));
    });
  }

  checkPerms(perm:any) {
    return new Promise((resolve) => {
      if(this.platform.is('android')) {
        this.perms.checkPermission(perm).then((data:any) => {
          if(data.hasPermission) resolve({granted:true});
          else this.perms.requestPermission(perm).then(() => resolve({granted:true}))
        })
      } else resolve({granted:true});
    });
  }

  checkDependencies(element) { //element is section or question
    // console.log('checking dependencies', element);
    if (element.dependencies) {
      for (let key in element.dependencies) {
        if(typeof element.dependencies[key] == 'object'){
          for(let o=0;o<element.dependencies[key].length;o++){
            if(this.formData[key] == element.dependencies[key][o] || (this.formData[key]!="" && element.dependencies[key][o] == "*")){
              element.active = true;
              return true;
            }
          }
        }
        if(typeof this.formData[key] == 'object' && this.formData[key] != null){
          for(let o=0;o<this.formData[key].length;o++){
            if(this.formData[key][o] == element.dependencies[key] || (this.formData[key][o]!=""&& element.dependencies[key] == "*")){
              element.active = true;
              return true;
            }
          }
        }
        //displays if the dependency matches
        if(this.formData[key] == element.dependencies[key]  || (this.formData[key]!="" && element.dependencies[key] == "*")) {
          element.active = true;
          return true;
        }
      }
    }
    else {
      element.active = true;
      return true;
    }
  }

  checkCheckbox(question, option){
    if(this.formData[question.id]){
      for(let i=0;i<this.formData[question.id].length;i++){
        if(option.id == this.formData[question.id][i]) return true;
      }
    }
    return false;
  }

  selectCheckbox(question, option){
    if(this.formData[question.id] == undefined) this.formData[question.id] = [];
    let index = -1;
    for(let i=0;i<this.formData[question.id].length;i++){
      if(option.id == this.formData[question.id][i]) index = i;
    }
    if(index == -1) this.formData[question.id].push(option.id);
    else this.formData[question.id].splice(index, 1)
    this.saveForm();
  }

  async submitSurvey(form){
    console.log('submit survey form', form, this.formData);
    let loader = await this.loadingCtrl.create({
      spinner: 'circles',
    });
    loader.present();

    //convert emojis
    for(let k in form.value){
      let ranges = [
        '\ud83c[\udf00-\udfff]', // U+1F300 to U+1F3FF
        '\ud83d[\udc00-\ude4f]', // U+1F400 to U+1F64F
        '\ud83d[\ude80-\udeff]' // U+1F680 to U+1F6FF
        ];
      let str = form.value[k];
      if(str) str = str.replace(new RegExp(ranges.join('|'), 'g'), '');
      this.formData[k] = str;
    }

    if(this.network.type == 'none') {
      let alert = await this.alertCtrl.create({
        header: 'Your device is not connected to the internet',
        message: 'Your survey wasn\'t submitted properly.',
        buttons: ['Okay']
      });
      alert.present();
      loader.dismiss();
    }
    else{
      let errors = '';

      //check required fields
      for(let k in this.formData){
        if(!this.formData[k]){
          for (let i=0; i<this.form.sections.length; i++) {
            let questions = this.form.sections[i].questions;
            if (!questions) continue;
            for (let j=0; j<questions.length; j++) {
              if (this.checkDependencies(questions[j]) && questions[j].id == k && questions[j].question_type != "Hidden" && questions[j].required == "1") {
                errors += '<p>'+questions[j].question+'</p>';
              }
            }
          }
        }
      }

      //check required images
      for (let i=0; i<this.form.sections.length; i++) {
        let questions = this.form.sections[i].questions;
        if (!questions) continue;
        for (let j=0; j<questions.length; j++) {
          if(questions[j].question_type == 'File' && this.checkDependencies(questions[j]) && !this.formData[questions[j].id] && questions[j].required == '1'){
            errors += '<p>'+questions[j].question+'</p>';
          }
        }
      }

      // this.ds.post('surveys/submit', this.formData, false).then((data: any) => {
      //   console.log('survey submit return data, ', data);
      // });
    }
  }
}
