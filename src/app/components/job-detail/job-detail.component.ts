import { Component, Input, OnInit } from '@angular/core';
import { DataService } from '../../data.service';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-job-detail',
  templateUrl: './job-detail.component.html',
  styleUrls: ['./job-detail.component.scss'],
})
export class JobDetailComponent implements OnInit {
  viewBid: boolean = false;
  @Input() job_id: number;
  // job: any;
  job: any = {
    job_id: 12,
    job_type: 'gardening',
    job_title: 'Build me a garden',
    description: 'I want a garden in my back yard to be able to grow fruits and vegetables, as well as flowers. I would like the beds to be raised, and cover most of the back yard, with connected stone paths around them all.',
    job_address: '128 15th Ave',
    job_address_2: '',
    job_city: 'Gotham City',
    job_state: 'NY',
    job_zip: '12010',
    job_location_type: 'residental', // might also be a drop down of choices
    status: 'Taking Bids', // might also be a set list of statuses, also be set on the back end
    owner: 10, // will be the customer that posted the job
    posted: "2/20/21", // will be the customer that posted the job
  };


  bidItem: any = {
    bid_price: 0,
    bid_description: '',
    job_owner: this.job.owner,
    // bid_placer: this.ds.load('user').id
    bid_placer: 15
  };

  success: string;
  error: string;

  constructor(
    public ds: DataService,
    public modalCtrl: ModalController,
    ) { }

  ngOnInit() {
    console.log('in job detail', this.job.job_id);
    // this.ds.get('job_detail/'+this.job_id).then((data:any) => {
    //   this.job = data.job;
    // });
  }
  

  openBid() {
    this.viewBid = !this.viewBid;
  }

  bid() {
    console.log('sending bid', this.bidItem);
    // this.ds.post('bid', this.bidItem).then((data:any) => {
    //   if (data.success) {
    //     this.success = 'Your bid has been placed successfully!';
    //     this.modalCtrl.dismiss()
    //   }
    //   else {
    //     this.error = 'There was an error placing your bid, please try again!';
    //   }
    // });
  }
}
