import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TabsMenuComponent } from './tabs-menu/tabs-menu.component';
import { PostJobFeildsComponent } from './post-job-feilds/post-job-feilds.component';
import { ScheduleFeildsComponent } from './schedule-feilds/schedule-feilds.component';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule
  ],
  declarations: [TabsMenuComponent, PostJobFeildsComponent, ScheduleFeildsComponent ],
  exports:[TabsMenuComponent, PostJobFeildsComponent, ScheduleFeildsComponent]
})
export class ComponentsModule {}
