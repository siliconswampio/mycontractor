import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';


@Component({
  selector: 'app-schedule-feilds',
  templateUrl: './schedule-feilds.component.html',
  styleUrls: ['./schedule-feilds.component.scss'],
})
export class ScheduleFeildsComponent implements OnInit {
  
  scheduleForm: FormGroup;

  day: any;
  days: any [] = [];

  jobs: any = [
    {
      job_id: 12,
      job_type: 'gardening',
      job_title: 'Build me a garden',
      description: 'I want a garden in my back yard to be able to grow fruits and vegetables, as well as flowers. I would like the beds to be raised, and cover most of the back yard, with connected stone paths around them all.',
      job_address: '128 15th Ave',
      job_address_2: '',
      job_city: 'Gotham City',
      job_state: 'NY',
      job_zip: '12010',
      job_location_type: 'residental', // might also be a drop down of choices
      status: 'Taking Bids' // might also be a set list of statuses, also be set on the back end
    },
    {
      job_id: 11,
      job_type: 'building',
      job_title: 'Kitchen redo',
      description: 'I need a new set of cabinets, a new island, and granite countertops.',
      job_address: '125 10th Ave',
      job_address_2: '',
      job_city: 'Gotham City',
      job_state: 'NY',
      job_zip: '12010',
      job_location_type: 'residental', // might also be a drop down of choices
      status: 'Taking Bids' // might also be a set list of statuses, also be set on the back end
    },
    {
      job_id: 10,
      job_type: 'roofing',
      job_title: 'Updating',
      description: 'I need to redo my roof for insurance',
      job_address: '2019 Manatee Ave',
      job_address_2: '',
      job_city: 'Bradenton',
      job_state: 'Fl',
      job_zip: '34238',
      job_location_type: 'rental', // might also be a drop down of choices
      status: 'Taking Bids' // might also be a set list of statuses, also be set on the back end
    }
  ];

  newDate: any;

  constructor(
    public modalCtrl: ModalController
  ) {
  
      this.scheduleForm = new FormGroup({
        jobTitle: new FormControl('', Validators.required),
        jobDate: new FormControl('', Validators.required)
      });
   }

  ngOnInit() {
    let dateFormate = new Date();
    let year = dateFormate.getFullYear();
    let date = dateFormate.getDate();
    let month = dateFormate.getMonth();

    for(let i=0; i<30; i++){
    this.day = new Date(year, month, date + i);
    console.log(this.day);
    this.days.push(this.day.toLocaleString([], {year: 'numeric', month: 'numeric', day: 'numeric'}));
  }
   
  }

  onSubmit() {
  //Should send post request to endpoint to create an appointment in the db.
  if(this.scheduleForm.valid){
    this.modalCtrl.dismiss(this.scheduleForm.value);
  } else { 
    console.log('Error!');
  }
  }

}
