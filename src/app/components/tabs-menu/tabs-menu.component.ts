import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../data.service';
import { JobListPage } from '../../job-list/job-list.page';


@Component({
  selector: 'app-tabs-menu',
  templateUrl: './tabs-menu.component.html',
  styleUrls: ['./tabs-menu.component.scss'],
})
export class TabsMenuComponent implements OnInit {
  user:any;

  

  constructor(public router: Router,	private ds: DataService) { }

  btnClicked(page){
    if(page == '/dashboard') {
      if(this.ds.load('user').user_type == 'Provider'){
        this.router.navigateByUrl('/dashboard');
      } else {
        this.router.navigateByUrl('/customer-dashboard');
      }
    } else {
      this.router.navigateByUrl(page);
    }
  }
  ngOnInit() {}

}

