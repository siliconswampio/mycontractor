import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-my-jobs',
  templateUrl: './my-jobs.page.html',
  styleUrls: ['./my-jobs.page.scss'],
})
export class MyJobsPage implements OnInit {

  myJobs: any =[

    {
      job_id: 12,
      job_type: 'Build me a garden',
      status: 'In progress',
      amount: '$500',
      date: '12/13/2021'
    },
    {
      job_id: 11,
      job_type: 'Kitchen redo',
      status: 'In progress',
      amount: '$9,000',
      date: '5/13/2021'
    },
    {
      job_id: 10,
      job_type: 'Lay Pipe',
      status: 'In progress',
      amount: '$1,000',
      date: '5/13/2021'
    }
  ];

  pendingJobs: any = [
    {
      job_id: 13,
      job_type:'Paint cabinets',
      status: 'Closed'
    },
    {
      job_id:14,
      job_type:'Fix garage door',
      status: 'Closed'
    },
    {
      job_id:15,
      job_type:'Build shower',
      status: 'Closed'
    }

  ];

  closedJobs: any = [
    {
      job_id: 13,
      job_type:'Paint cabinets',
      status: 'Closed'
    },
    {
      job_id:14,
      job_type:'Fix garage door',
      status: 'Closed'
    },
    {
      job_id:15,
      job_type:'Build shower',
      status: 'Closed'
    }

  ];

  constructor(
    public ds: DataService,
  ) { }

  ngOnInit() {
  }

}
