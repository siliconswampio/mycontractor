import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-job-post',
  templateUrl: './job-post.page.html',
  styleUrls: ['./job-post.page.scss'],
})
export class JobPostPage implements OnInit {
  job: any = {
    job_id: 0,
    job_type: '',
    job_title: '',
    description: '',
    job_address: '',
    job_address_2: '',
    job_city: '',
    job_state: '',
    job_zip: '',
    job_location_type: '', // might also be a drop down of choices
    status: '' // might also be a set list of statuses, also be set on the back end
  };
  success: string;

  constructor(
    public ds: DataService,
  ) { }

  ngOnInit() {
  }

  submit() {
    let data = this.job;
    console.log(data);
    this.success = 'You have successfully posted a job, you will recieve a confirmation email and text shortly.';

    /*
    this.ds.post('consumer_job_posting', data).then((data:any) => {
      console.log('job_posted', data);
      if (data.error) {
        this.postError = data.error;
      }
      else {
        this.success = data.success;
        // this might need to be handled slightly differently once we get things working and connected
        this.modalCtrl.create({
          component: JobDetailComponent,
          swipeToClose: true,
          componentProps: {
            job_id: data.job_id
          }
        }).then(modal => modal.present());
        }
      }) 
    */
  }
  
// Toggle for job feilds componet
public show: boolean = false;
public buttonName: any = true;

toggle() {
    this.show = !this.show;
    if(this.show)
        this.buttonName = false;
    else
        this.buttonName = true;
}

}
