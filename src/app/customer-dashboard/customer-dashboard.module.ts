import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CustomerDashboardPageRoutingModule } from './customer-dashboard-routing.module';

import { CustomerDashboardPage } from './customer-dashboard.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CustomerDashboardPageRoutingModule,
    ComponentsModule
  ],
  declarations: [CustomerDashboardPage]
})
export class CustomerDashboardPageModule {}
